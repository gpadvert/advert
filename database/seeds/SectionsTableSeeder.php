<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
        DB::table('sections')->delete();

        DB::table('sections')->insert([
                ['id' => '1',  'name' => 'اطعمه',],
                ['id' => '2','name' => 'عربات',],
                ['id' => '3','name' => 'عطور',],
                ['id' => '4','name' => 'الكترونيات',],
                ['id' => '5','name' => 'الموضة',],
                 ['id' => '6','name' => 'صناعات يدوية',],
                 ['id' => '7','name' => 'اخرى',],
            ]);
    }
}
