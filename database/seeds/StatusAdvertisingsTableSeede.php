<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class StatusAdvertisingsTableSeede extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status_advertisings')->delete();

        DB::table('status_advertisings')->insert([
                ['id' => '1',  'name' => 'جديد',],
                ['id' => '2','name' =>'منتهي',],
                ['id' => '3','name' => 'بلوك',],

            ]);
    }
    
}
