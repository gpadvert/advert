<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(SectionsTableSeeder::class);
         $this->call(StatusUsersTableSeeder::class);
         $this->call(StatusAdvertisingsTableSeede::class);
         $this->call(TypeUsersTableSeeder::class);


    }
}
