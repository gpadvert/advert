<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class StatusUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status_users')->delete();

        DB::table('status_users')->insert([
                ['id' => '1',  'name' => 'فعال',],
                ['id' => '2','name' =>'متوقف',],
                ['id' => '3','name' => 'بلوك',],

            ]);
    }
}
