<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TypeUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('type_users')->delete();

      DB::table('type_users')->insert([
              ['id' => '1',  'name' => 'أدمن',],
              ['id' => '2','name' =>'معلن',],
              ['id' => '3','name' => 'زائر',],

          ]);
    }
}
