@extends('layouts.admin')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">اضافة تذكرة جديدة</div>
                    <form  action="{{ url('/ticket/store') }}"  method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body">
                              <div class="form-group ">
                                <label for="title">عنوان التذكرة</label>
                                <input type="text" class="form-control" name="tic_title" value="{{ old('tic_title') }}" required dir="rtl" >
                              </div>

                              <div class="form-group ">
                                <label for="detalis">التفاصيل</label>
                                <textarea class="form-control" aria-label="With textarea"  name="tic_detalis" value="{{ old('tic_detalis') }}" rows="4" required dir="rtl"></textarea>
                              </div>

                            </div>
                            <div class="card-footer text-left">
                              <button type="submit" class="btn btn-secondary">
                                حفظ
                              </button>
                              <a href="{{url('/myTicket')}}"class="btn btn-danger">
                                  الغاء
                              </a>
                            </div>

                          </form>

</div>
</div>
</div>
</div>
@endsection
