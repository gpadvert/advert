@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> تذكراتي</div>
                <div class="card-body">
                  <div class="text-right">
                  <a class="btn btn-dark" href="{{url('/ticket/create')}}">اضافة تذكرة</a>
                  </div>
                  <br>
                  @if (count($tickets) >0)
                  <table class="table table-striped text-center">

                        <tr>
                          <th scope="col"></th>
                          <th scope="col">العنوان</th>
                        </tr>
                          @foreach ($tickets as $ticket )
                              <tr>
                                <td >
                                    <a class="btn btn-outline-danger"href="{{url('/ticket/delete/'.$ticket->id)}}">حذف</a>
                                </td>
                                <td><a href=" {{url('/ticket/show/'.$ticket->id)}}">{{$ticket->title}}</a></td>
                              </tr>
                          @endforeach
                    </table>
                  @else
                    لا يوجد تذاكر
                  @endif

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
