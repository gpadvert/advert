@extends('layouts.admin')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="alert alert-dark col-md-12 text-center" role="alert">
                    عنوان ل التذكرة
                    </div>
                    <div class="alert alert-light col-md-12 text-center" role="alert">
                      {{$ticket->title}}
                    </div>
                    <div class="alert alert-dark col-md-12 text-center" role="alert">
                      تفاصيل التذكرة
                    </div>
                    <div class="alert alert-light col-md-12 text-center" role="alert">
                      {{$ticket->detalis}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
<div class="card-header">الردود</div>
<div class="card-body">

  <table class="table table-striped text-center">

        <tr>
          <th scope="col">الوقت</th>
          <th scope="col">التعليق</th>
         <th scope="col">الاسم</th>
        </tr>
          @foreach ($ticket->comments as $comment )
              <tr>
                <td>{{$comment->created_at->format('d m Y - H:i:s')}}</td>
                <td>{{$comment->comment}}</td>
                <td>{{$comment->user->name}}</td>

              </tr>
          @endforeach
    </table>


  </div>
  <form  action="{{ url('/comment/store') }}"  method="post">
      @csrf
        <input type="hidden" name="id" value="{{$ticket->id }}">
<div class="card-header">رد على التذكرة</div>
<div class="card-body">
      <div class="form-group ">
          <textarea class="form-control" aria-label="With textarea" name="comment"  rows="4" required dir="rtl"></textarea>
        <br>
        <div class="text-right">
          <button type="submit" class="btn btn-secondary">
              رد
          </button>
        </div>
    </div>
</div>

                  <div class="card-footer text-right">
                    <a href="{{url('/myTicket')}}"class="btn btn-secondary">
                        رجوع
                    </a>
                  </div>
                </div>
            </div>
        </div>
    </div>


@endsection
