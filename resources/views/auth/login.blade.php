@extends('layouts.simpleLayout')

@section('content')

<div class="row justify-content-center ">
<div class="col-md-4 card card-login mt-5">
  <div class=" card-header text-center mt-3">{{ __('تسجيل الدخول') }}</div>
  <div class="card-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group text-right">
           <label for="email">{{ __('البريد الالكتروني') }}</label>
           <input type="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required >
        </div>
        <div class="form-group text-right">
           <label for="password">{{ __('كلمة المرور') }}</label>
           <input type="password"   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required >
        </div>
      <button type="submit" class="btn btn-dark">
          {{ __('تسجيل الدخول') }}
      </button>
    </form>
  </div>
</div>
</div>


@endsection
