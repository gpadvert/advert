@extends('layouts.admin')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-10">
             <div class="card">
                  <div class="card-header">تعديل البيانات</div>
                      <form  action="{{ url('/profile/update') }}"  method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$profile->id}}">
                           <div class="card-body">

                             <div class="form-group ">
                               <label for="name">الاسم</label>
                               <input type="text" class="form-control" name="name" value="{{$profile->name}}"  dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="phone">رقم التواصل</label>
                               <input type="text" class="form-control" name="phone" value="{{$profile->phone}}"  dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="web">رابط الموقع</label>
                               <input type="text" class="form-control" name="web" value="{{$profile->web}}"  dir="rtl" >
                             </div>
                             <div class="form-group row">
                               <div class="col-md-5">
                                   <label for="image">تعديل الشعار</label>
                                   <input type="file" class="form-control-file"  name="prof_image"  dir="rtl" >
                               </div>
                               <div class="col-md-4">
                                   <img src="{{asset('uploads/'.$profile->image)}}" class="img-thumbnail" alt="" >
                               </div>
                               <div class="col-md-3">
                                 <label for="image" >الشعار</label>
                               </div>
                             </div>

                             <div class="form-group ">
                               <label for="address">العنوان</label>
                               <input type="text" class="form-control" name="address" value="{{$profile->address}}"  dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="city">المدينة</label>
                               <input type="text" class="form-control" name="city" value="{{$profile->city}}"  dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="country">الدولة</label>
                               <input type="text" class="form-control" name="country" value="{{$profile->country}}"  dir="rtl" >
                             </div>


                           </div>
                           <div class="card-footer text-left">
                             <button type="submit" class="btn btn-secondary">
                                حفظ التعديلات
                             </button>
                             <a href="{{url('/home')}}"class="btn btn-danger">
                            الغاء
                             </a>
                           </div>

                         </form>

</div>
</div>
</div>
</div>
@endsection
