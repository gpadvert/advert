@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">بياناتي</div>

                <div class="card-body">

                  <br>
                الاسم : {{$profile->name}}
                  </br>
                  <br>
            رقم التواصل : {{$profile->phone}}
                  </br>
                  <br>
                رابط الموقع : {{$profile->web}}
                  </br>
                  <br>
                شعار الاعلان : {{$profile->image}}
                  </br>
                  <br>
                العنوان : {{$profile->address}}
                  </br>
                  <br>
                المدينة : {{$profile->city}}
                  </br>
                  <br>
                الدولة : {{$profile->country}}
                  </br>
                  <br>
                  <div class="form-group row mb-0">
                      <div class="col-md-6 offset-md-4">
                          <button type="submit" class="btn btn-primary">
                              تعديل
                          </button>


                      </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
