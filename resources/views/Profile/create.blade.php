@extends('layouts.admin')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-10">
             <div class="card">
                  <div class="card-header">بيانات المعلن</div>
                      <form  action="{{ url('/profile/store') }}"  method="post" enctype="multipart/form-data">
                        @csrf
                           <div class="card-body">

                             <div class="form-group ">
                               <label for="name">الاسم</label>
                               <input type="text" class="form-control" name="name" value="{{ old('name') }}" required dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="phone">رقم التواصل</label>
                               <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="web">رابط الموقع</label>
                               <input type="text" class="form-control" name="web" value="{{ old('web') }}" required dir="rtl" >
                             </div>

                             <div class="form-group">
                               <label for="image">شعار الاعلان</label>
                               <input type="file" class="form-control-file"  name="prof_image" required dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="address">العنوان</label>
                               <input type="text" class="form-control" name="address" value="{{ old('address') }}" required dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="city">المدينة</label>
                               <input type="text" class="form-control" name="city" value="{{ old('city') }}" required dir="rtl" >
                             </div>

                             <div class="form-group ">
                               <label for="country">الدولة</label>
                               <input type="text" class="form-control" name="country" value="{{ old('country') }}" required dir="rtl" >
                             </div>


                           </div>
                           <div class="card-footer text-left">
                             <button type="submit" class="btn btn-secondary">
                                 حفظ
                             </button>
                           </div>

                         </form>

</div>
</div>
</div>
</div>
@endsection
