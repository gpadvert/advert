@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header ">المستخدمين</div>
                <div class="card-body">
                  <div class="text-right">
                  <a class="btn btn-dark" href="{{url('/users/create')}}">اضافة مستخدم</a>
                  </div>
                  <br>
                  @if (count($users) >0)


                  <table class="table table-striped text-center">

                  <tr>
                    <th scope="col"></th>
                      <th scope="col">الاشتراكات</th>
                    <th scope="col">الحاله</th>
                    <th scope="col">النوع</th>
                    <th scope="col">الاسم</th>




                  </tr>
                    @foreach ($users as  $user)
                    <tr>
                      <td>

                        @if($user->status_user != 3)
                        <a class="btn btn-outline-danger"href="{{url('/users/blockUser/'.$user->id)}}">حظر</a>
                        @endif
                        @if($user->status_user != 1)
                        <a class="btn btn-outline-secondary" href="{{url('/users/userActive/'.$user->id)}}" >فعال</a>
                        @endif
                        @if($user->status_user != 2)
                        <a class="btn btn-outline-secondary" href="{{url('/users/userUnACtiva/'.$user->id)}}">معطل</a>
                        @endif
                            </td>
                            <td>
                              @if($user->type_user == 2)
                                @if($user->profile->subscription == 0)
                                  <a class="btn btn-outline-danger"href="{{url('/users/subscription/'.$user->id)}}">تفعيل اشتراك</a>
                                @else
                                 تم الاشتراك
                                @endif
                              @else
                              ---
                              @endif
                            </td>
                            <td>

                              @switch($user->status_user)
                                  @case(1)
                                      فعال
                                      @break

                                  @case(2)
                                      معطل
                                      @break
                                  @case(3)
                                      بلوك
                                      @break

                                  @default
                                      غير معروف
                                  @endswitch
                            </td>
                            <td>{{$user->typeUser->name}}</td>
                            <td><a href=" {{url('/users/show/'.$user->id)}}">{{$user->name}}</td>





                          </tr>
                    @endforeach


              </table>
@else
  لا يوجد مستخدمين
  @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
