@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> تفاصيل المستخدم</div>

                <div class="card-body">

                  <br>
                  {{$user->name}} :   الأسم
                  </br>
                  <br>
             {{$user->email}}  :  البريد الالكتروني
                  </br>
                  <br>
                  نوع المستخدم : {{$user->typeUser->name}}
                  </br>
                  <br>
                  حالة المستخدم : {{$user->statusUser->name}}
                  </br>
                  <br>
                  <a class="btn btn-outline-secondary" href="{{url('/users')}}">رجوع</a>
</br>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
