<ul class="sidebar navbar-nav bg-dark ">
  @if(Auth::user()->type_user==1)
<li class="nav-item active ">
  <a class="nav navbar-brand" href="{{url('/users')}}">
    <span class="text-white">المستخدمين</span>
  </a>
</li>
<li class="nav-item active ">
  <a class="nav navbar-brand" href="{{url('/AdvControl')}}">
    <span class="text-white">الاعلانات</span>
  </a>
</li>
<li class="nav-item active ">
  <a class="nav navbar-brand" href="{{url('/tickets')}}">
    <span class="text-white">الدعم الفني </span>
  </a>
</li>
@elseif(Auth::user()->type_user==2)
  <li class="nav-item active ">
    <a class="nav navbar-brand" href="{{url('/profile')}}">
      <span class="text-white ">الملف الشخصي  </span>
    </a>
  </li>
  <li class="nav-item active ">
    <a class="nav navbar-brand" href="{{url('/advertising')}}">
      <span class="text-white">اعلاناتي </span>
    </a>
  </li>
@elseif(Auth::user()->type_user==3)
  <a class="nav navbar-brand" href="{{url('/favorite')}}">
    <span class="text-white">مفضلتي </span>
  </a>
</li>
<li class="nav-item active ">
  <a class="nav navbar-brand" href="{{url('/evaluation')}}">
    <span class="text-white">تقييماتي </span>
  </a>
</li>
@endif
@if(Auth::user()->type_user!=1)

<li class="nav-item active ">
  <a class="nav navbar-brand" href="{{url('/myTicket')}}">
    <span class="text-white">تذاكري </span>
  </a>
</li>
@endif
</ul>
