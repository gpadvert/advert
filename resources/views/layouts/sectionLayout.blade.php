<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('/css/index.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://unpkg.com/ionicons@4.4.4/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">


        <title>اعلانكم</title>
  </head>


  <body>
    <div class="container">
      <div class="row hidden-xs topper">
        <div class="col-xs-7 col-sm-7">
          <ul class="nav navbar-nav navbar-left hidden-xs">
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('تسجيل الدخول ') }}</a>
                      </li>
                        @else

                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu " >
                            <a class="dropdown-item" href="{{ route('home') }}">
                                {{ __('التحكم ') }}
                            </a>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('خروج ') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
    </ul>
    </div>
        <div class="col-xs-3 col-xs-offset-4 col-sm-2 col-sm-offset-5 ">
          <a href="{{url('/') }}"><img am-TopLogo alt="logo"  src="{{ asset('/images/logo.png') }}" class="img-responsive"></a>
        </div>
      </div> <!-- End Topper -->
      <!-- Navigation -->
      <div class="row">
        <nav class="navbar navbar-inverse" role="navigation">
          <div class="container">
                  <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">

              <ul class="nav navbar-nav js-nav-add-active-class navbar-right mt-5">
                <li><a href="{{url('/section/7')}}">اخرى</a></li>
                <li><a href="{{url('/section/6')}}">صناعات يدوية</a></li>
                <li><a href="{{url('/section/5')}}">الموضة</a></li>
                <li><a href="{{url('/section/4')}}">الكترونيات</a></li>
                <li><a href="{{url('/section/3')}}">عطور</a></li>
                <li><a href="{{url('/section/2')}}">عربات</a></li>
                <li><a href="{{url('/section/1')}}">أطعمه</a></li>
                <li class="active"><a href="{{url('/') }}">الرئيسية</a></li>
                  </ul>
            </div><!-- /.navbar-collapse -->
          </div>
        </nav>
      </div>
    </div>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    @yield('content')



    <footer>
      <div class="container">
        <div class="row">

          <div class="col-md-12 col-sm-12">
            <p class="wow fadeInUp"  data-wow-delay="0.3s">Copyright © Ealancom.com 2019- Designed by Naba & Maha</p>
            <ul class="social-icon wow fadeInUp"  data-wow-delay="0.6s">
              <li><a href="{{url('https://twitter.com/e3lancom3')}}" class="fa fa-twitter"></a></li>
              <li><a href="{{url('https://www.instagram.com/e3lancom.1')}}"class="fab fa-instagram"></a></li>
              <li><a href="{{url('/ticket/create')}}"class="fas fa-envelope"></a></li>
            </ul>
            <div class="social-icon">
              <ul id="nav">
                <li><a href="{{url('about')}}"> من نحن ؟</a></li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="https://ar.atbar.org/wp-includes/js/wp-embed.min.js?ver=4.9.9"></script>
<script type="text/javascript" src="https://ar.atbar.org/wp-content/plugins/jetpack/modules/wpgroho.js?ver=4.9.9"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
