@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> تقيماتي</div>
                <div class="card-body">
                  @if (count($evaluation) > 0)
                <table class="table table-striped text-center">
                  <tr>
                    <th scope="col"></th>
                      <th scope="col">التقيم</th>
                    <th scope="col">الاعلان</th>
                  </tr>
                    @foreach ($evaluation as  $row)
                    <tr>
                        <td>
                            <a class="btn btn-outline-danger" href="{{url('/evaluation/delete/'.$row->id)}}">حذف</a>
                        </td>
                          <td>{{$row->evaluation}}</td>
                        <td>{{$row->advertising->title}}</td>
                    </tr>
                    @endforeach
                 </table>
                  @else
                  لا يوجد تقيمات
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
