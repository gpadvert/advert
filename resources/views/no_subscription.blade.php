@extends('layouts.admin')

@section('content')
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading" >عفوا</h4>
  <p>غير مسموح لك اضافة اعلان . بسبب تجاوز الحد المجاني من الإعلانات </p>
  <hr>
  <p class="mb-0">يرجى التواصل مع ادارة الموقع عن طريق تذاكري واطلب اشتراك </p>
</div>
@endsection
