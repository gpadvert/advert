@extends('layouts.admin')

@section('content')
<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading" >عفوا</h4>
  <p>غير مسموح لك الوصول . بسبب عدم إضافة الملف الشخصي لك </p>
  <hr>
  <p class="mb-0">يرجى إضافة ملفك الشخصي عن طريق الضغط على الملف الشخصي في القائمة الجانبية </p>
</div>
@endsection
