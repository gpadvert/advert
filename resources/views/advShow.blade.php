
@extends('layouts.sectionLayout')
@section('content')
<div class="container">
  <div class="row">
    <a href="{{url('advinfo/'.$advertising->user->id)}}">
    <div class="col-sm-2 col-md-2 mb-5">
             <img src="{{asset('uploads/'.$advertising->user->profile->image)}}"
             alt="" class="img-rounded img-responsive" />
         </div>
         <div class="col-sm-4 col-md-4">
             <blockquote>
                 <p>{{$advertising->user->name}}</p> <small><cite >{{$advertising->user->profile->city}}  <i class="glyphicon glyphicon-map-marker"></i></cite></small>
             </blockquote>
             <p> <i class="glyphicon glyphicon-phone"></i> {{$advertising->user->profile->phone}}

         </div>
       </div>
       </a>
<hr>
       <div class="container">
         <div class="row justify-content-center">
           <h2 class="text-center">{{$advertising->title}} <span>-{{$advertising->section->name}}</span></h2>
<hr>
       <figure class="snip1139 hover">
         <blockquote>{{$advertising->title}}
           <div class="arrow"></div>
         </blockquote>
         <img src="{{asset('uploads/'.$advertising->master_image)}}" alt="sample3"/>

         <div class="author">
         </div>
       </figure>

              <figure class="snip1139">
                <blockquote>{{$advertising->detalis}}
                  <div class="arrow"></div>
                </blockquote>
                <img src="{{asset('uploads/'.$advertising->image)}}" alt="sample47"/>
                <div class="author">
                </div>
              </figure>

  </div>
</div>
</div>


  @endsection
