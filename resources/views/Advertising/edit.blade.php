@extends('layouts.admin')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">تعديل الأعلان</div>
                    <form  action="{{ url('/advertising/update') }}"  method="post" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="id" value="{{ $advertising->id }}">
                  <div class="card-body">
                            <div class="form-group ">
                              <label for="title">عنوان الأعلان</label>
                              <input type="text" class="form-control" name="adv_title" value="{{ $advertising->adv_title }}"  dir="rtl" >
                            </div>
                            <div class="form-group ">
                              <label for="detalis">وصف الأعلان</label>
                              <textarea class="form-control" aria-label="With textarea"  name="adv_detalis"   dir="rtl">{{$advertising->adv_detalis}}</textarea>
                            </div>
                            <div class="form-group ">
                              <label for="section_id">القسم</label>
                              <select class="form-control" name="section_id" dir="rtl" >
                                <option selected>اختار...</option>

                                  @foreach ($sections as $section)
                                    <option value="{{$section->id}}" {{($section->id == $advertising->section_id) ? 'selected' :''}} >{{$section->name}} </option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-5">
                                  <label for="master_image">تعديل الصورة الرئيسية</label>
                                  <input type="file" class="form-control-file"  name="adv_master_image"  dir="rtl" >
                              </div>
                              <div class="col-md-4">
                                  <img src="{{asset('uploads/'.$advertising->master_image)}}" class="img-thumbnail" alt="" >
                              </div>
                              <div class="col-md-3">
                                <label for="master_image" >الصورة الرئيسية</label>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-5">
                                  <label for="image">تعديل الصورة </label>
                                  <input type="file" class="form-control-file"  name="adv_image"  dir="rtl" >
                              </div>
                              <div class="col-md-4">
                                  <img src="{{asset('uploads/'.$advertising->image)}}" class="img-thumbnail" alt="" >
                              </div>
                              <div class="col-md-3">
                                <label for="image" >الصورة </label>
                              </div>
                            </div>

                          </div>
                          <div class="card-footer text-left">
                            <button type="submit" class="btn btn-secondary">
                              حفظ
                            </button>
                            <a href="{{url('/advertising')}}"class="btn btn-danger">
                                الغاء
                            </a>
                          </div>

                          </form>

                          </div>
                          </div>
                          </div>
                          </div>
@endsection
