@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> تفاصيل الاعلان</div>

              <table class="table table-borderless">

                <thead>
                <tr>
                <th scope="col"></th>
                <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                   <tr>
                     <th scope="row"></th>
                      <td>{{$advertising->title}}</td>
                     <th>عنوان الاعلان</th>
                   </tr>
                   <tr>
                     <th scope="row"></th>
                     <td>{{$advertising->detalis}}</td>
                     <th>وصف الاعلان</th>
                   </tr>
                   <tr>
                     <th scope="row"></th>
                     <td>{{$advertising->section->name}}</td>
                     <th>القسم</th>
                   </tr>
                   <tr>
                     <th scope="row"></th>
                      <td>  <img src="{{asset('uploads/'.$advertising->master_image)}}" class="img-thumbnail" width="200" height="200" ></td>
                     <th>الصورة الرئيسية</th>
                   </tr>
                   <tr>
                     <th scope="row"></th>
                      <td> <img src="{{asset('uploads/'.$advertising->image)}}"class="img-thumbnail"width="200" height="200" ></td>
                     <th>الصورة </th>
                   </tr>
                 </tbody>
               </table>


                  <div class="card-footer text-left">
                    <a href="{{url('/advertising')}}"class="btn btn-secondary">
                        رجوع
                    </a>
                  </div>
                </div>
            </div>
        </div>
    </div>


@endsection
