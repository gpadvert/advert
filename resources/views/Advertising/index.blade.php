@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">اعلاناتي</div>
                <div class="card-body">
                  <div class="text-right">
                  <a class="btn btn-dark" href="{{url('/advertising/create')}}">اضافة أعلان</a>
                  </div>
                  <br>
                  @if (count($advertising) >0)
                  <table class="table table-striped text-center">

                        <tr>
                          <th scope="col"></th>
                          <th scope="col">الحالة</th>
                          <th scope="col">القسم</th>
                          <th scope="col">العنوان</th>
                        </tr>
                          @foreach ($advertising as  $row)
                              <tr>
                                <td >
                                    <a class="btn btn-outline-danger"href="{{url('/advertising/delete/'.$row->id)}}">حذف</a>
                                    @if($row->status_advertising == 1)
                                      <a class="btn btn-outline-danger" href="{{url('/advertising/advUnActiva/'.$row->id)}}">تعطيل </a>
                                    @elseif($row->status_advertising == 2)
                                      <a class="btn btn-outline-success" href="{{url('/advertising/advActive/'.$row->id)}}">تفعيل </a>
                                    @endif
                                    <a class="btn btn-outline-secondary" href="{{url('/advertising/edit/'.$row->id)}}">تعديل</a>
                                </td>
                                <td>

                                  @switch($row->status_advertising)
                                      @case(1)
                                          فعال
                                          @break

                                      @case(2)
                                          معطل
                                          @break
                                      @case(3)
                                          محظور
                                          @break

                                      @default
                                          غير معروف
                                      @endswitch
                                </td>
                                <td>{{$row->section->name}}</td>
                                <td><a href=" {{url('/advertising/show/'.$row->id)}}">{{$row->title}}</a></td>
                              </tr>
                          @endforeach
                    </table>
                  @else
                    لا يوجد اعلانات
                  @endif

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
