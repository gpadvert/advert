@extends('layouts.admin')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card text-left">
                <div class="card-header">اضافة اعلان جديد</div>
                    <form  action="{{ url('/advertising/store') }}"  method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body">
                              <div class="form-group text-left ">
                                <label for="title ">عنوان الأعلان</label>
                                <input type="text" class="form-control" name="adv_title" value="{{ old('adv_title') }}" required dir="rtl" >
                              </div>

                              <div class="form-group text-left ">
                                <label for="detalis">وصف الأعلان</label>
                                <textarea class="form-control" aria-label="With textarea"  name="adv_detalis" value="{{ old('adv_detalis') }}" required dir="rtl"></textarea>
                              </div>

                              <div class="form-group text-left">
                                <label for="section_id">القسم</label>
                                <select class="form-control" name="section_id"  required dir="rtl" >
                                  <option selected>اختار...</option>
                                    @foreach ($sections as $section)
                                      <option value="{{$section->id}}" >{{$section->name}} </option>
                                    @endforeach
                                </select>
                              </div>

                              <div class="form-group text-left">
                                <label for="master_image">الصورة الرئيسية</label>
                                <input type="file" class="form-control-file"  name="adv_master_image" required dir="rtl" >
                              </div>

                              <div class="form-group text-left">
                                <label for="image">الصورة</label>
                                <input type="file" class="form-control-file"  name="adv_image" required dir="rtl">
                              </div>


                            </div>
                            <div class="card-footer text-left">

                              <a href="{{url('/advertising')}}"class="btn btn-danger">
                                الغاء
                              </a>
                              <button type="submit" class="btn btn-secondary">
                                  حفظ
                              </button>
                            </div>

                          </form>

</div>
</div>
</div>
</div>
@endsection
