<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('/css/index.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://unpkg.com/ionicons@4.4.4/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <title>اعلانكم</title>

  </head>


  <body>
    <div class="container">
      <div class="row hidden-xs topper">
        <div class="col-xs-7 col-sm-7">
          <ul class="nav navbar-nav navbar-left hidden-xs">
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('تسجيل الدخول ') }}</a>
                      </li>
                        @else

                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu " >
                            <a class="dropdown-item" href="{{ route('home') }}">
                                {{ __('حسابي  ') }}
                            </a>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('خروج ') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
    </ul>
    </div>
        <div class="col-xs-3 col-xs-offset-4 col-sm-2 col-sm-offset-5 ">
          <a href="{{url('/') }}"><img am-TopLogo alt="logo"  src="{{ asset('/images/logo.png') }}" class="img-responsive"></a>
        </div>
      </div> <!-- End Topper -->
      <!-- Navigation -->
      <div class="row">
        <nav class="navbar navbar-inverse" role="navigation">
          <div class="container">
                  <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">

              <ul class="nav navbar-nav js-nav-add-active-class navbar-right">
                <li><a href="{{url('/section/7')}}">اخرى</a></li>
                <li><a href="{{url('/section/6')}}">صناعات يدوية</a></li>
                <li><a href="{{url('/section/5')}}">الموضة</a></li>
                <li><a href="{{url('/section/4')}}">الكترونيات</a></li>
                <li><a href="{{url('/section/3')}}">عطور</a></li>
                <li><a href="{{url('/section/2')}}">عربات</a></li>
                <li><a href="{{url('/section/1')}}">أطعمه</a></li>
                <li class="active"><a href="{{url('/') }}">الرئيسية</a></li>
                  </ul>
            </div><!-- /.navbar-collapse -->
          </div>
        </nav>
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section id="header" class="header-one">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
              <div class="header-thumb">
                  <h3 class="wow fadeInUp" data-wow-delay="1.9s"> منصة اعلانات للمشاريع الناشئة </h3>
              </div>
    			</div>
    		</div>
    	</div>
    </section>

    <div class="container">

      @foreach ($sections as $section)
      <div class="p-5 mb-3 bg-dark text-white">
      <h3 class="text-center ">
      {{$section->name}}
      <hr>
      </h3>
      </div>
      <div class="row">
    @foreach ($section->getFiveAdvertising as $advertising)
    <div class=" col-md-4">
    <div class="block">
    <div class="top">
      <ul>
        <li>
          <form  action="{{ url('/favorite/store') }}"  method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $advertising->id }}">
              <button type="submit" class="btn btn-secondary">
              @guest
                <i class="fa fa-heart-o"></i>
              @else
                @if(!empty($advertising->getFavorite(Auth::user()->id)))
                  <i class="fa fa-heart"></i>
                @else
                  <i class="fa fa-heart-o"></i>
                @endif
              @endguest

              </button>
              {{$advertising->countFavorite()}}
          </form>

          <a href="{{url('advinfo/'.$advertising->user->id)}}">

        <li><span class="converse text-right"> {{$advertising->user->name}} المعلن</span></li>
      </a>
</li>
      </ul>
    </div>
        <div class="middle">
          <a href="{{url('/advShow/'.$advertising->id)}}">

      <img src="{{asset('uploads/'.$advertising->master_image)}}" alt="pic" />
    </div>
    <div class="bottom">
      <div class="heading">{{$advertising->title}}</div>
        <li>
          @for ($i = 5; $i > 0; $i--)
            <a href="{{url('/evaluation/store',['id'=>$advertising->id,'ev'=>$i])}}">
              @guest
                <i class="fa fa-star-o" ></i>
              @else
                @if(!empty($advertising->getEvaluation(Auth::user()->id)->evaluation))
                  @if($i <= $advertising->getEvaluation(Auth::user()->id)->evaluation)
                    <i class="fa fa-star" ></i>
                  @else
                    <i class="fa fa-star-o" ></i>
                  @endif
                @else
                  <i class="fa fa-star-o" ></i>
                @endif
              @endguest
            </a>
          @endfor
        </br>
        @foreach ($advertising->countEvaluation()  as  $value)
            {{ $value}}
        @endforeach
        </li>

      </div>
    </div>
      </div>

    </a>
    @endforeach

          </div>


  <div class="text-left ">
    <p><a class="moer btn btn-secondary " href="{{url('/section/'.$section->id)}}">المزيد »</a>
  </div>
<br>
<hr>
@endforeach

  </div>


  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <p class="footer wow fadeInUp"  data-wow-delay="0.3s">Copyright © Ealancom.com 2019- Designed by Naba & Maha</p>
          <ul class="social-icon wow fadeInUp"  data-wow-delay="0.6s">
            <li><a href="{{url('https://twitter.com/e3lancom3')}}" class="fa fa-twitter"></a></li>
            <li><a href="{{url('https://www.instagram.com/e3lancom.1')}}"class="fab fa-instagram"></a></li>
            <li><a href="{{url('/ticket/create')}}"class="fas fa-envelope"></a></li>
          </ul>
          <div class="social-icon">
            <ul id="nav">
              <li><a href="{{url('about')}}"> من نحن ؟</a></li>
            </ul>
          </div>
        </div>

      </div>
    </div>
  </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://ar.atbar.org/wp-includes/js/comment-reply.min.js?ver=4.9.10"></script>
<script type="text/javascript" src="https://s0.wp.com/wp-content/js/devicepx-jetpack.js?ver=201911"></script>
<script type="text/javascript" src="https://secure.gravatar.com/js/gprofiles.js?ver=2019Maraa"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
