@extends('layouts.aboutLayout')

@section('content')




<div class="container">

<div class="jumbotron">
<div class="row justify-content-center">

      <div class="col-4 ">
        <div class="aboutus-content text-center ">
<h1>من نحن </h1>
<h4>طالبات كلية علوم الحاسب والمعلومات - جامعة الاميرة نوره بنت عبدالرحمن - عملنا على مشروع تخرج يهدف الى خدمة اصحاب المشاريع الناشئة بتسهيل عرض اعلاناتهم تحت منصة مخصصة فقط بالإعلانات ..
   يتيح الموقع للمعلنين باقات اعلانات ولوحة تحكم خاصه لإدارة الاعلانات ،
خدمات التفضيل للإعلانات وتقييمها .
</h4>

        </div>
</div>
    </div>

  </div>
</div>

<div class="container">
        <div class="row justify-content-center">
            <div class="column">
                <div class="box">
                    <div class="title">
                        <i class="fa fa-paper-plane"></i>
                        <h2>الاساسية</h2>
                    </div>
                    <div class="price">
                        <h4><sup></sup>FREE</h4>
                    </div>
                    <div class="option">
                        <ul>
                            <li><i class="fa fa-check"></i> فقط 5 اعلانات</li>
                          </ul>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="box">
                    <div class="title">
                        <i class="fa fa-rocket"></i>
                        <h2> باقة استثنائي</h2>
                    </div>
                    <div class="price">
                        <h4><sup>SR</sup>100</h4>
                    </div>
                    <div class="option">
                        <ul>
                          <li><i class="fa fa-check"></i> بلا حدود</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<br><br><br>
    	 <div class="col col-sm-6">
            <div class="CustomCard hoverCustomCard">
                <div class="CustomCardheader text-white">
                    <h5 class="col pt-2"><strong> ارقام الحسابات الخاصة بتفعيل الاشتراكات </strong></h5>
                  </div>
                <div class="avatar">
                    <img alt="" src="{{ asset('/images/logo.png') }}" height="50px" width="50px">
                </div>
                <div class="info">
                    <div class="desc"> " الراجحي : " 585608010171471 </div>
                    <div class="desc">" الرياض : " 2330401469940 </div>
<br>
                    <footer class="blockquote-footer float-right m-5"> يتم التحويل على الحسابات اعلاه وفتح تذكره للدعم الفني لتأكيد تفعيل الاشتراك <cite title="Source Title"></cite></footer>
                </div>

            </div>
        </div>



@endsection
