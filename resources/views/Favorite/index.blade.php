@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> مفضلاتي</div>
                <div class="card-body">
                  @if (count($favorites) > 0)
                  <table class="table table-striped text-center">
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">الاعلان</th>
                  </tr>
                    @foreach ($favorites as  $row)
                    <tr>
                        <td>
                            <a class="btn btn-outline-danger" href="{{url('/favorite/delete/'.$row->id)}}">حذف</a>
                        </td>
                        <td>{{$row->advertising->title}}</td>
                    </tr>
                    @endforeach
                 </table>
                  @else
                لا يوجد مفضلات
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
