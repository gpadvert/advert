
@extends('layouts.sectionLayout')



@section('content')


<div class="container">
       <div class="row">
           <div class="panel panel-default">
           <div class="panel-heading text-right">  <h4 >بيانات المعلن </h4></div>
            <div class="panel-body">
           <div class="col-md-4 col-xs-8 col-sm-6 col-lg-4">
            <img alt="User Pic" src="{{asset('uploads/'.$user->profile->image)}}" id="profile-image1" class="img-circle img-responsive">
           </div>
           <div class="col-md-8 col-xs-7 col-sm-6 col-lg-8" >
               <div class="container" >
                 <li><h2><span class="glyphicon glyphicon-user one" style="width:50px;"></span>{{$user->profile->name}}</h2></li>
               </div>
                 <hr>
               <ul class="container details" >
                 <li><p><span class="glyphicon glyphicon-phone" style="width:50px;"></span>{{$user->profile->phone}}</p></li>
                 <li><p><span class="glyphicon glyphicon-envelope one" style="width:50px;"></span>{{$user->email}}</p></li>
               </ul>
               <hr>
               <div class="col-sm-5 col-xs-6 tital " >{{$user->profile->country}}</div>
               <div class="col-sm-5 col-xs-6 tital " >{{$user->profile->city}}</div>
               <div class="col-sm-5 col-xs-6 tital " >{{$user->profile->address}}</div>

           </div>
     </div>
 </div>
 </div>
</div>


    <div class="container">
      <hr>
      <h3 class="mt-3 pb-3 mb-4 font-italic border-bottom text-center"> الاعلانات  </h3>
      <hr>
      <div class="row">
    @foreach ($user->advertisings as $advertising)
    <a href="{{url('/advShow/'.$advertising->id)}}">
    <div class=" col-md-4">
    <div class="block">
    <div class="top">
      <ul>
        <li>
          <form  action="{{ url('/favorite/store') }}"  method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $advertising->id }}">
              <button type="submit" class="btn btn-secondary">
              @guest
                <i class="fa fa-heart-o"></i>
              @else
                @if(!empty($advertising->getFavorite(Auth::user()->id)))
                  <i class="fa fa-heart"></i>
                @else
                  <i class="fa fa-heart-o"></i>
                @endif
              @endguest

              </button>
              {{$advertising->countFavorite()}}
          </form>


        <li><span class="converse text-right"> {{$advertising->user->name}} المعلن</span></li>
</li>
      </ul>
    </div>
        <div class="middle">
      <img src="{{asset('uploads/'.$advertising->master_image)}}" alt="pic" />
    </div>
    <div class="bottom">
      <div class="heading">{{$advertising->title}}</div>
        <li>
          @for ($i = 5; $i > 0; $i--)
            <a href="{{url('/evaluation/store',['id'=>$advertising->id,'ev'=>$i])}}">
              @guest
                <i class="fa fa-star-o" ></i>
              @else
                @if(!empty($advertising->getEvaluation(Auth::user()->id)->evaluation))
                  @if($i <= $advertising->getEvaluation(Auth::user()->id)->evaluation)
                    <i class="fa fa-star" ></i>
                  @else
                    <i class="fa fa-star-o" ></i>
                  @endif
                @else
                  <i class="fa fa-star-o" ></i>
                @endif
              @endguest
            </a>
          @endfor
        </br>
        @foreach ($advertising->countEvaluation()  as  $value)
            {{ $value}}
        @endforeach
        </li>

      </div>
    </div>
      </div>
    </a>
    @endforeach
          </div>
          <hr>

  </div>

    @endsection
