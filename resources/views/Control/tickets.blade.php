@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> تذاكر الدعم الفني </div>
                <div class="card-body">
                  <br>
                  @if (count($tickets) >0)
                  <table class="table table-striped text-center">

                        <tr>
                          <th scope="col"></th>
                          <th scope="col">الاسم </th>
                          <th scope="col">العنوان</th>
                        </tr>
                          @foreach ($tickets as $ticket )
                              <tr>
                                <td ><a href=" {{url('/ticketShow/'.$ticket->id)}}">عرض</a></td>
                                <td>{{$ticket->user->name}}</td>
                                <td>{{$ticket->title}}</td>
                              </tr>
                          @endforeach
                    </table>
                  @else
                    لا يوجد تذاكر
                  @endif

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
