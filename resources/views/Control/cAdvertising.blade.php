@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"> اعلانات</div>
                <div class="card-body">
                  @if (count($advertising) >0)
                  <table class="table table-bordered text-center">
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">الحالة</th>
                          <th scope="col">القسم</th>
                          <th scope="col">العنوان</th>
                        </tr>
                          @foreach ($advertising as  $row)
                              <tr>
                                <td >
                                    @if($row->status_advertising != 3)
                                    <a class="btn btn-outline-danger"href="{{url('/AdvControl/blockAdv/'.$row->id)}}">حظر</a>
                                    @endif
                                    @if($row->status_advertising != 2)
                                    <a class="btn btn-outline-secondary" href="{{url('/AdvControl/stopAdv/'.$row->id)}}">معطل</a>
                                    @endif
                                    @if($row->status_advertising != 1)
                                    <a class="btn btn-outline-secondary" href="{{url('/AdvControl/newAdv/'.$row->id)}}">فعال</a>
                                    @endif
                                </td>
                                  <td>

                                    @switch($row->status_advertising)
                                        @case(1)
                                            فعال
                                            @break

                                        @case(2)
                                            معطل
                                            @break
                                        @case(3)
                                            بلوك
                                            @break

                                        @default
                                            غير معروف
                                        @endswitch
                                  </td>
                                <td>{{$row->section->name}}</td>
                                <td>{{$row->title}}</td>
                              </tr>
                          @endforeach
                    </table>
                  @else
                    لا يوجد اعلانات
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
