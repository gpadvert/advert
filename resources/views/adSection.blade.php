@extends('layouts.sectionLayout')

@section('content')

<div class="container">

 <div class="col-sm-6 col-md-10 affix-content">
   <hr>

         <h3 class=" font-italic border-bottom text-center">
         {{$section->name}}
         <hr>

         </h3>

         <div class="row">
       @foreach ($section->advertisings as $advertising)
       <a href="{{url('/advShow/'.$advertising->id)}}">
              <div class=" col-md-4">
       <div class="block">
       <div class="top">
         <ul>
           <li>
             <form  action="{{ url('/favorite/store') }}"  method="post" enctype="multipart/form-data">
           @csrf
           <input type="hidden" name="id" value="{{ $advertising->id }}">
           <button type="submit" class="btn btn-secondary">
               <i class="far fa-heart"></i>
           </button>
         </form>

           <li><span class="converse text-right"> {{$advertising->user->name}} المعلن</span></li>
   </li>
         </ul>
       </div>
           <div class="middle">
         <img src="{{asset('uploads/'.$advertising->master_image)}}" alt="pic" />
       </div>
       <div class="bottom">
         <div class="heading">{{$advertising->title}}</div>
         <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i>
         <i class="fa fa-star-o" aria-hidden="true"></i>
       <i class="fa fa-star-o" aria-hidden="true"></i>
     <i class="fa fa-star-o" aria-hidden="true"></i>
     <i class="fa fa-star-o" aria-hidden="true"></i></a></li>
       </div>
     </div>
         </div>
       @endforeach
     </a>
             </div>
     <br>
   <hr>
     </div>
     <div class="col-sm-3 col-md-2 affix-sidebar">
   <div class="sidebar-nav">
  <div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <span class="visible-xs navbar-brand">Sidebar menu</span>
    </div>
    <div class="navbar-collapse collapse sidebar-navbar-collapse">
     <div class="navbar-collapse collapse sidebar-navbar-collapse">
       <ul class="nav navbar-nav" id="sidenav01">
         <li> <h3> <span class="glyphicon glyphicon-hand-down"></span> الاقسام </h3> </li>
         @foreach ($allSections as $section)
         <li><a href="{{url('/section/'.$section->id)}}">  {{$section->name}} </a></li>
         @endforeach
       </ul>
       </div>
      </div><!--/.nav-collapse -->
    </div>
  </div>
 </div>
</div>



@endsection
