<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Evaluation;
use Auth;

class EvaluationController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('visitor'); //user visitor
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $evaluation = Evaluation::where('user_id',Auth::user()->id)->get();
      return view('Evaluation.index',compact('evaluation'));
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$ev)
    {
        Evaluation::updateOrCreate(
            ['advertising_id' => $id, 'user_id' => Auth::user()->id],
            ['evaluation' => $ev ]
        );
        return back();
    }




  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $evaluation = Evaluation::find($id);
          $evaluation->advertising_id = $request->input('advertising_id');
          $evaluation->user_id = Auth::user()->id;

            $evaluation->save();
          return redirect('/evaluation');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evaluation  = Evaluation::find($id)->delete();
      return redirect('/evaluation ');

        //
    }
}
