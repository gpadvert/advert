<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Profile;
use App\Model\User;
use Auth;
use File;
use Validator;
class ProfileController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('advertiser');//user advertiser
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->has_profile == 1){
          return redirect('profile/edit');
        }else{
          return redirect('profile/create');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      Validator::make($request->all(),[
        'prof_image'=>'required',
        'phone'=>'required|min:3|max:255',
        'web'=>'required|min:3|max:255',
        'address'=>'required|min:3|max:255',
        'city'=>'required|min:3|max:255',
        'country'=>'required|min:3|max:255',
        'name'=>'required|min:3|max:255',
      ])->validate();

      $profile =new Profile ();
            $profile->user_id = Auth::user()->id;
            //uplodes image
                  $destinationPath = public_path('uploads');
                  $file = $request->file('prof_image');
                  $extension = $file->getClientOriginalExtension();
                  $fileName = uniqid().'.'. time() .'.'.$extension;
                  $file->move($destinationPath, $fileName);
            $profile->image =$fileName;
            $profile->phone = $request->input('phone');
            $profile->web = $request->input('web');
            $profile->address = $request->input('address');
            $profile->city = $request->input('city');
            $profile->country = $request->input('country');
            $profile->name = $request->input('name');
            $profile->save();
            $user =User::find(Auth::user()->id);
            $user->has_profile = 1;
            $user->save();
            return redirect('/home');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
      $profile = Profile::where('user_id',Auth::user()->id)->first();
          return view('Profile.edit',compact('profile'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      Validator::make($request->all(),[

        'phone'=>'required|min:5|max:255',
        'web'=>'required|min:3|max:255',
        'address'=>'required|min:5|max:255',
        'city'=>'required|min:3|max:255',
        'country'=>'required|min:3|max:255',
      ])->validate();

      $profile = Profile::where('user_id',Auth::user()->id)->first();

            if(!empty($request->file('prof_image'))){
                  $destinationPath = public_path('uploads');
                  $file = $request->file('prof_image');
                  $extension = $file->getClientOriginalExtension();
                  $fileName = uniqid().'.'. time() .'.'.$extension;
                  $file->move($destinationPath, $fileName);
            $profile->image =$fileName;
          }
            $profile->phone = $request->input('phone');
            $profile->web = $request->input('web');
            $profile->address = $request->input('address');
            $profile->city = $request->input('city');
            $profile->country = $request->input('country');
            $profile->save();
            return redirect('/home');
        //
    }

  
}
