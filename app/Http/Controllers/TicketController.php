<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Ticket;
use App\Model\Comment;
use Auth;
use Validator;
class TicketController extends Controller

{
  public function __construct()
  {
      $this->middleware('auth');

  }


     public function createTicket()
     {

       return view('Ticket.create');
     }

     public function storeTicket(Request $request)
     {
       Validator::make($request->all(),[
         'tic_title'=>'required|min:5|max:255',
         'tic_detalis'=>'required|min:5',

       ])->validate();


       $ticket=new Ticket();
       $ticket->user_id= Auth::user()->id;
       $ticket->title= $request->input('tic_title');
       $ticket->detalis= $request->input('tic_detalis');

       $ticket->save();
       return redirect('/myTicket');
     }

     public function myTicket()
     {

       $tickets= Ticket::where('user_id',Auth::user()->id)->get();

         return view('Ticket.myTicket',compact('tickets'));
     }

    public function show($id)
    {

          $ticket= Ticket::find($id);
          if($ticket->user_id=Auth::user()->id){
            return view('Ticket.show',compact('ticket'));

          }else {
            return redirect('/unauthorized');
          }

    }




    public function storeComment(Request $request)
    {
      Validator::make($request->all(),[
        'comment'=>'required|min:5',
      ])->validate();

      $comment=new Comment();
      $comment->user_id = Auth::user()->id;
      $comment->ticket_id = $request->input('id');
      $comment->comment = $request->input('comment');

      $comment->save();
      return redirect('/ticket/show/'.$request->input('id'));

    }
    public function destroy($id)
    {
      $ticket= Ticket::find($id);
      if($ticket->user_id=Auth::user()->id){
        Comment::where('ticket_id',$ticket->id)->delete();
        $ticket->delete();
        return redirect('/myTicket');

      }else {
        return redirect('/unauthorized');
      }

}

}
