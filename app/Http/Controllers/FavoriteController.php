<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Favorite;

use Auth;


class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *

     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
         $this->middleware('visitor'); //user visitor
     }

    public function index()
    {
        $favorites=Favorite::where('user_id',Auth::user()->id)->get();
        return view('Favorite.index',compact('favorites'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $isFavorite = Favorite::where('user_id',Auth::user()->id)->where('advertising_id', $request->input('id'))->first();
         if(!empty($isFavorite)){
          $isFavorite->delete();
         }else{
           $favorite = new Favorite();
           $favorite->advertising_id = $request->input('id');
           $favorite->user_id =Auth::user()->id;
           $favorite->save();
         }
         return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $favorite  = Favorite::find($id)->delete();
        return redirect('/favorite ');
    }
}
