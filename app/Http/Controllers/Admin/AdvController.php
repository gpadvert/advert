<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Advertising;
use Auth;

class AdvController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('admin'); //user admin
    //
}
       public function cAdvertising()
       {
           $advertising= Advertising::all();
           return view('Control.cAdvertising',compact('advertising'));

       }
       public function stopAdv($id)
       {
           $advstatus= Advertising::find($id);
           $advstatus->status_advertising = 2 ;
           $advstatus->save();
           return redirect('/AdvControl');
       }
       public function blockAdv($id){
         $advstatus= Advertising::find($id);
         $advstatus->status_advertising = 3 ;
         $advstatus->save();
         return redirect('/AdvControl');
      }
      public function newAdv($id){
        $advstatus= Advertising::find($id);
        $advstatus->status_advertising = 1 ;
        $advstatus->save();
        return redirect('/AdvControl');
     }


}
