<?php

namespace App\Http\Controllers\Admin;
use Auth;
use App\Model\Ticket;
use App\Model\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminTicketController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('admin'); //user admin
    //
}
  public function index()
{
    $tickets = Ticket::all();
    return view('Control.tickets',compact('tickets'));

}
  public function ticketShow($id)
{
      $ticket= Ticket::find($id);
        return view('Control.ticketShow',compact('ticket'));
}


   public function storeAdminComment(Request $request)
{
     $comment=new Comment();
     $comment->user_id = Auth::user()->id;
     $comment->ticket_id = $request->input('id');
     $comment->comment = $request->input('comment');

     $comment->save();
  return redirect('/ticketShow/'.$request->input('id'));

}

}
