<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Model\User;
use Auth;
use Hash;
use  App\Model\Profile;

class UserController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');

        $this->middleware('admin'); //user admin
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
    return view('User.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    return view('User.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $user = new User();
      $user->password = Hash::make($request->input('password'));
      $user->email =$request->input('email');
      $user->name = $request->input('name');
      $user->type_user = $request->input('type_user');
      $user->save();
      return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::find($id);
        return view('User.show',compact('user'));

    }

    public function userUnACtiva($id)
    {
        $admin= User::find($id);
        $admin->status_user = 2 ;
        $admin->save();
        return redirect('/users');
    }
    public function userActive($id)
    {
        $visitor= User::find($id);
        $visitor->status_user = 1 ;
        $visitor->save();
        return redirect('/users');
    }
    public function blockUser($id)
    {
        $visitor= User::find($id);
        $visitor->status_user = 3 ;
        $visitor->save();
        return redirect('/users');
    }

    public function subscription($id)
    {

        $profile = Profile::where('user_id',$id)->first();
        $profile->subscription = 1 ;
        $profile->save();
        return redirect('/users');
    }
}
