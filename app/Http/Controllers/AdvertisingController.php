<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Advertising;
use App\Model\Section;
use App\Model\Evaluation;
use App\Model\Favorite;
use Auth;
use File;
use Validator;
use App\Model\Profile;

class AdvertisingController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('advertiser');//user advertiser
      $this->middleware('hasProfile');
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $advertising= Advertising::where('user_id',Auth::user()->id)->get();
       return view('Advertising.index',compact('advertising'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $countAdvUser =Advertising::where('user_id',Auth::user()->id)->count();
      // dd($countAdvUser);
    $check =  Profile::where('user_id',Auth::user()->id)->first();
      // dd($check ,$countAdvUser);

      if($countAdvUser > 4 AND ($check->subscription) == 0){
        return view('no_subscription');
      }
      else{
        $sections = Section::all();
        return view('Advertising.create',compact('sections'));

      }
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      Validator::make($request->all(),[
        'adv_title'=>'required|min:3|max:255',
        'adv_detalis'=>'required|min:3',
        'adv_image'=>'required',
        'adv_master_image'=>'required',
        'section_id'=>'required',
      ])->validate();

      // dd($request->input());
          $advertising =new Advertising();
          $advertising->title = $request->input('adv_title');
          $advertising->detalis = $request->input('adv_detalis');
          $advertising->user_id = Auth::user()->id;
              //uplodes image
              $destinationPath = public_path('uploads');
              $file = $request->file('adv_image');
              $extension = $file->getClientOriginalExtension();
              $fileName = uniqid().'.'. time() .'.'.$extension;
              $file->move($destinationPath, $fileName);
               $advertising->image =$fileName;
              //uplodes master_image
              $destinationPath = public_path('uploads');
              $file = $request->file('adv_master_image');
              $extension = $file->getClientOriginalExtension();
              $fileName = uniqid().'.'. time() .'.'.$extension;
              $file->move($destinationPath, $fileName);
              $advertising->master_image = $fileName;
              $advertising->section_id = $request->input('section_id');
               $advertising->status_advertising = 1;

              $advertising->save();
              return redirect('/advertising');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertising = Advertising::find($id);
        if($advertising->user_id=Auth::user()->id){
          return view('Advertising.show',compact('advertising'));

        }else {
          return redirect('/unauthorized');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $advertising = Advertising::find($id);
        $sections = Section::all();

        if($advertising->user_id=Auth::user()->id){
        return view('Advertising.edit',compact('sections','advertising'));
        }else {
          return redirect('/unauthorized');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      Validator::make($request->all(),[
        'adv_title'=>'required|min:3|max:255',
        'adv_detalis'=>'required|min:3',
        'adv_image'=>'required',
        'adv_master_image'=>'required',
        'section_id'=>'required',
      ])->validate();
        $advertising = Advertising::find($request->input('id'));
        $advertising->title = $request->input('adv_title');
        $advertising->detalis = $request->input('adv_detalis');
        $advertising->user_id = Auth::user()->id;
            //uplodes image
      if(!empty($request->file('adv_image'))){
            $destinationPath = public_path('uploads');
            $file = $request->file('adv_image');
            $extension = $file->getClientOriginalExtension();
            $fileName = uniqid().'.'. time() .'.'.$extension;
            $file->move($destinationPath, $fileName);
            $advertising->image =$fileName;
          }
            //uplodes master_image
    if(!empty($request->file('adv_master_image'))){
            $destinationPath = public_path('uploads');
            $file = $request->file('adv_master_image');
            $extension = $file->getClientOriginalExtension();
            $fileName = uniqid().'.'. time() .'.'.$extension;
            $file->move($destinationPath, $fileName);
            $advertising->master_image = $fileName;
          }
          $advertising->section_id = $request->input('section_id');
            $advertising->status_advertising = 1;
            $advertising->save();

                return redirect('/advertising');

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $advertising = Advertising::find($id);
         Evaluation::where('advertising_id',$advertising->id)->delete();
         Favorite::where('advertising_id',$advertising->id)->delete();
        $advertising->delete();
        return redirect('/advertising');
    }

    public function advUnActiva($id)
    {
      $advertising = Advertising::find($id);
      if($advertising->user_id = Auth::user()->id){
        $advstatus= Advertising::find($id);
        $advstatus->status_advertising = 2 ;
        $advstatus->save();
        return redirect('/advertising');
      }
        return redirect('/unauthorized');
    }

    public function advActive($id){
      $advertising = Advertising::find($id);
      if($advertising->user_id = Auth::user()->id){
        $advstatus= Advertising::find($id);
        $advstatus->status_advertising = 1 ;
        $advstatus->save();
        return redirect('/advertising');
      }
        return redirect('/unauthorized');
   }


}
