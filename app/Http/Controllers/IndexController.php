<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Section;
use App\Model\Advertising;
use App\Model\User;
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sections = Section::all();
      $countAdvertising = Advertising::all()->count();
      $countAdvertiser = User::where('type_user',2)->count();
      $countVisitor = User::where('type_user',3)->count();
      return view('index',compact('sections','countAdvertising','countAdvertiser','countVisitor'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function section($id)
    {
      $section = Section::find($id);
      $allSections = Section::all();
      return view('adSection',compact('section','allSections'));
    }



  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function advShow($id)
  {
    $advertising=Advertising::find($id);
    return view('advShow',compact('advertising'));
  }

  public function advinfo($id)
  {
  $user=User::find($id);
  return view('advinfo',compact('user'));

  }

}
