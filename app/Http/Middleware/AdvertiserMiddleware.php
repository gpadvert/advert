<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdvertiserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
       if ( Auth::check() &&  Auth::user()->type_user != 2)
       {
           return redirect('/unauthorized');
       }
         return $next($request);
     }
}
