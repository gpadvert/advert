<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ( Auth::check() &&  Auth::user()->type_user == 2 && Auth::user()->has_profile == 0)
      {
          return redirect('/noProfile');
      }
        return $next($request);
    }
}
