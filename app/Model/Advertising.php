<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
  protected $fillable = [
      'title','user_id','image','detalis','master_image','section_id','status_advertising'
  ];
  /**
    * Get the post that owns the comment.
    */
   public function user()
   {
       return $this->belongsTo('App\Model\User');
   }
   /**
   * Get the post that owns the comment.
   */
  public function section()
  {
      return $this->belongsTo('App\Model\Section');
  }

  public function statusAdvertising()
  {
      return $this->belongsTo('App\Model\StatusAdvertising','status_user');
  }
  public function favorite()
  {
      return $this->hasMany('App\Model\Favorite');
  }
  public function countFavorite()
  {
      return $this->favorite()->count();
  }
  public function evaluation()
  {
      return $this->hasMany('App\Model\Evaluation');
  }

  public function countEvaluation()
  {
      for($i = 5; $i > 0; $i--){
        $count[$i] = $this->evaluation()->where('evaluation',$i)->count();
      }
      return $count;
  }

  public function getEvaluation($id)
  {
      $get_evaluation = $this->evaluation()->where('user_id',$id)->first();
      return $get_evaluation;
  }

  public function getFavorite($id)
  {
       return $this->favorite()->where('user_id',$id)->first();
  }
}
