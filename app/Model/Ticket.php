<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $fillable = [
        'detalis','user_id','title','status'
    ];
    public function comments()
    {
        return $this->hasMany('App\Model\Comment');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\user');
    }
}
