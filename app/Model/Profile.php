<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $fillable = [
    'user_id','image','phone','web','address','city','country','name','subscription'
  ];
  /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
