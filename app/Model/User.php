<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type_user','has_profile','status_user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function advertisings()
    {
        return $this->hasMany('App\Model\Advertising');
    }

   public function profile()
   {
       return $this->hasOne('App\Model\Profile');
   }

    public function statusUser()
    {
        return $this->belongsTo('App\Model\StatusUser','status_user');
    }
    public function typeUser()
    {
        return $this->belongsTo('App\Model\TypeUser','type_user');
    }

    public function favorite()
    {
        return $this->hasMany('App\Model\Favorite');
    }
    public function evaluation()
    {
        return $this->hasMany('App\Model\Evaluation');
    }
    public function comment()
    {
        return $this->hasMany('App\Model\Comment');
    }
    public function ticket()
    {
        return $this->hasMany('App\Model\Ticket');
    }
}
