<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusAdvertising extends Model
{
  protected $fillable = [
      'name'
  ];
  
  public function advertisings()
  {
      return $this->hasMany('App\Model\Advertising');
  }
}
