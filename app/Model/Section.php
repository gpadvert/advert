<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
  protected $fillable = [
      'name'
  ];
  /**
   * Get the comments for the blog post.
   */
  public function advertisings()
  {
      return $this->hasMany('App\Model\Advertising')->orderBy('created_at','dasc');
  }
  public function getFiveAdvertising()
  {
    return $this->advertisings()->limit(3);



  }
}
