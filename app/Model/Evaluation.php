<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
  protected $fillable = [
      'advertising_id','user_id','evaluation'
  ];
  public function advertising()
  {
      return $this->belongsTo('App\Model\Advertising');
  }
  public function user()
  {
      return $this->belongsTo('App\Model\User');
  }

}
