<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'comment','user_id','ticket_id'
    ];

    public function ticket()
    {
        return $this->belongsTo('App\Model\Ticket');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

}
