<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//
//     return view('welcome');
//
// });
Route::get('/unauthorized', function () {
    return view('unauthorized');
});


Route::get('/noProfile', function () {
    return view('noProfile');
});

Route::get('/about', function () {

    return view('about');

});
Route::get('/create', function () {

    return view('/Ticket/create');

});

Route::get('/ticket/delete/{id}', 'TicketController@destroy');
Route::post('/comment/store', 'TicketController@storeComment');
Route::get('/ticket/show/{id}', 'TicketController@show');
Route::post('/ticket/store', 'TicketController@storeTicket');
Route::get('/ticket/create', 'TicketController@createTicket');
Route::get('/myTicket', 'TicketController@myTicket');
Route::post('/storeAdminComment', 'Admin\AdminTicketController@storeAdminComment');
Route::get('/ticketShow/{id}', 'Admin\AdminTicketController@ticketShow');
Route::get('/tickets', 'Admin\AdminTicketController@index');

Auth::routes();

Route::get('/', 'IndexController@index')->name('index');
Route::get('/section/{id}','IndexController@section');
Route::get('/advShow/{id}', 'IndexController@advShow');
Route::get('/advinfo/{id}', 'IndexController@advinfo');

Route::group([
  'prefix' => 'evaluation'
], function(){
Route::get('store/{id}/{ev}', 'EvaluationController@store');
Route::get('delete/{id}', 'EvaluationController@destroy');
Route::get('/', 'EvaluationController@index');
Route::post('update/{id}', 'EvaluationController@update');
});

Route::group([
  'prefix' => 'profile'
], function(){
Route::get('create', 'ProfileController@create');
Route::post('store', 'ProfileController@store');
Route::get('edit', 'ProfileController@edit');
Route::post('update', 'ProfileController@update');
Route::get('/', 'ProfileController@index');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group([
  'prefix' => 'advertising'
], function(){
Route::get('create', 'AdvertisingController@create');
Route::post('store', 'AdvertisingController@store');
Route::get('show/{id}', 'AdvertisingController@show');
Route::get('delete/{id}', 'AdvertisingController@destroy');
Route::get('edit/{id}', 'AdvertisingController@edit');
Route::post('update', 'AdvertisingController@update');
Route::get('advUnActiva/{id}', 'AdvertisingController@advUnActiva');
Route::get('advActive/{id}', 'AdvertisingController@advActive');
Route::get('/', 'AdvertisingController@index');
});

Route::group([
  'prefix' => 'users'
], function(){

Route::get('/', 'Admin\UserController@index');
Route::get('show/{id}', 'Admin\UserController@show');
Route::get('create', 'Admin\UserController@create');
Route::post('store', 'Admin\UserController@store');
Route::get('blockUser/{id}', 'Admin\UserController@blockUser');
Route::get('userActive/{id}', 'Admin\UserController@userActive');
Route::get('userUnACtiva/{id}', 'Admin\UserController@userUnACtiva');
Route::get('subscription/{id}', 'Admin\UserController@subscription');


});

Route::group([
  'prefix' => 'favorite'
], function(){
Route::post('store', 'FavoriteController@store');
Route::get('delete/{id}', 'FavoriteController@destroy');
Route::get('/', 'FavoriteController@index');
});

Route::group([
  'prefix' => 'admin'
], function(){

Route::get('/', 'Admin\AdvController@cAdvertising');
Route::get('show/{id}', 'Admin\UserController@show');

});
Route::group([
  'prefix' => 'AdvControl'
], function(){

Route::get('/', 'Admin\AdvController@cAdvertising');
// Route::get('show/{id}', 'Admin\UserController@show');
Route::get('blockAdv/{id}', 'Admin\AdvController@blockAdv');
Route::get('stopAdv/{id}', 'Admin\AdvController@stopAdv');
Route::get('newAdv/{id}', 'Admin\AdvController@newAdv');

});
